
#pragma   once

#include  <sys/types.h>
#include  <sys/socket.h>
#include  <netinet/in.h>

#include  <errno.h>
#include  <string.h>
#include  <unistd.h>

#include  <list>
#include  <string>

#include  "BaseClass.hh"
#include  "SelectEntity.hh"

namespace Net
{
  class Select : public BaseClass
  {
    private:
      std::list<SelectEntity*>  _entities;
      int                       _timeout; /* milliseconds */

    public:
      Select(void);
      ~Select(void);

      void                      run(void);

      void                      addEntity(SelectEntity * entity);
      void                      removeEntity(SelectEntity * entity);

      int                       getTimeout(void) const;
      void                      setTimeout(int timeout);

    private:
      int                       _initFds(fd_set *read, fd_set *write, fd_set *err);
      void                      _select(void);
      void                      _procedeCallbacks(fd_set *read, fd_set *write, fd_set *err);
      virtual void              onError(void) const;
  };
}

