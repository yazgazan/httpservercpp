
#include  <errno.h>
#include  <string.h>
#include  <unistd.h>

#include  <algorithm>
#include  <iostream>

#include  "Select.hh"
#include  "globals.hh"

using namespace Net;

Select::Select(void)
  : _entities(),
    _timeout(TIMEOUT_WAIT)
{
}

Select::~Select(void)
{
}

void Select::run(void)
{
  while (!isError())
    _select();
}

int Select::_initFds(fd_set *read, fd_set *write, fd_set *err)
{
  int max = -1;

  FD_ZERO(read);
  FD_ZERO(write);
  FD_ZERO(err);

  for (std::list<SelectEntity*>::iterator it = _entities.begin();
      it != _entities.end();
      ++it)
  {
    SelectEntity  *entity = *it;
    int const     sock = entity->getFd();

    if (sock == -1 || entity->closed())
    {
      if (entity->getDeleteOnClose())
        delete entity;
      it = _entities.erase(it);
      --it;
      continue;
    }
    if (sock > max)
      max = sock;
    FD_SET(sock, read);
    FD_SET(sock, write);
    FD_SET(sock, err);
  }

  return max;
}

void Select::_select(void)
{
  fd_set  readFds;
  fd_set  writeFds;
  fd_set  errFds;
  int     err;
  int     maxFd = -1;

  if(isError())
    return;
  if (_entities.size() == 0)
  {
    _setError(e_no_entity);
    return;
  }

  maxFd = _initFds(&readFds, &writeFds, &errFds);
  err = select(maxFd + 1, &readFds, &writeFds, &errFds, NULL);

  if (err == -1)
  {
    _setError(std::string(strerror(errno)));
    return;
  }
  _procedeCallbacks(&readFds, &writeFds, &errFds);
}

void Select::_procedeCallbacks(fd_set *read, fd_set *write, fd_set *err)
{
  int n = 0;

  for (std::list<SelectEntity*>::iterator it = _entities.begin();
      it != _entities.end();
      ++it)
  {
    SelectEntity  *entity = *it;
    int const     sock = entity->getFd();
    bool      useDefault = true;

    if (FD_ISSET(sock, read))
    {
      entity->readAvailable();
      useDefault = false;
    }
    if (FD_ISSET(sock, write))
    {
      entity->writeAvailable();
      useDefault = false;
    }
    if (FD_ISSET(sock, err))
    {
      entity->errAvailable();
      useDefault = false;
    }
    if (useDefault)
      entity->defaultCallback();
    ++n;
  }
}

void Select::addEntity(SelectEntity * entity)
{
  std::list<SelectEntity*>::iterator findIter;
  
  findIter = std::find(_entities.begin(), _entities.end(), entity);

  if (findIter != _entities.end())
    return;
  _entities.push_back(entity);
}

void Select::removeEntity(SelectEntity * entity)
{
  std::list<SelectEntity*>::iterator findIter;
  
  findIter = std::find(_entities.begin(), _entities.end(), entity);

  if (findIter == _entities.end())
    return;
  _entities.erase(findIter);
}

int Select::getTimeout(void) const
{
  return _timeout;
}

void Select::setTimeout(int timeout)
{
  _timeout = timeout;
}

void Select::onError(void) const
{
  std::cerr << "Error : " << getError() << std::endl;
  exit(1);
}
