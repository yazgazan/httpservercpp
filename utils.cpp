
#include  <sstream>

#include  "utils.hh"

using namespace Net;


std::string Utils::trim(std::string const & input)
{
  std::string ret(input);

  if (ret.find(" ") == 0)
    ret = trim(ret.substr(1));
  if (ret.rfind(" ") == ret.size() - 2)
    ret = trim(ret.substr(0, ret.size() - 1));

  return ret;
}

std::string Utils::toString(int n)
{
  std::stringstream ss;

  ss << n;

  return ss.str();
}

std::string Utils::toString(long n)
{
  std::stringstream ss;

  ss << n;

  return ss.str();
}

