
#pragma   once

#include  <string>

namespace Net
{
  class Utils
  {
    public:
      static std::string trim(std::string const & input);
      static std::string toString(int n);
      static std::string toString(long n);
  };
}

