
#include <iostream>
#include <string>

#include "HttpSocket.hh"
#include "utils.hh"

using namespace Net;

HttpSocket::HttpSocket(int fd, struct sockaddr_in const & sin)
  : Socket(fd, sin),
  _headers(),
  _method(""),
  _path(""),
  _headersRecieved(false),
  _statusCode(200),
  _reason("OK"),
  _closeOnWrite(false),
  _headersSent(false),
  _repHeaders()
{
  setDeleteOnClose();
}

HttpSocket::~HttpSocket(void)
{
}

void HttpSocket::readAvailable(void)
{
  Socket::readAvailable();

  if (!_headersRecieved)
    parseHeader();
  else
    onBody();
}

void HttpSocket::parseHeader(void)
{
  size_t      endFirstLine;
  std::string line;

  endFirstLine = _buff.find("\r\n");
  if (endFirstLine == std::string::npos)
    return;
  line = _buff.substr(0, endFirstLine);
  if (_method == "")
    parseFirstLine(line);
  else
  {
    if (line.size() == 0)
    {
      _headersRecieved = true;
      initResHeaders();
      reqRecieved();
      return;
    }
    parseHeaderFields(line);
  }
  _buff = _buff.substr(endFirstLine + 2);
  if (_buff.size() > 0)
    parseHeader();
}

void HttpSocket::parseFirstLine(std::string const & line)
{
  size_t    firstSpace;
  size_t    secondSpace;

  firstSpace = line.find(" ");
  secondSpace = line.find(" ", firstSpace + 1);
  _method = line.substr(0, firstSpace);
  _path = line.substr(firstSpace + 1, secondSpace - firstSpace - 1);
}

void HttpSocket::parseHeaderFields(std::string const & line)
{
  size_t  colon;
  std::string key;
  std::string value;

  colon = line.find(":");
  key = Utils::trim(line.substr(0, colon));
  value = line.substr(colon + 1);
  _headers[key] = value;
}

void HttpSocket::initResHeaders(void)
{
  _repHeaders["Server"] = "custom Server";
  _repHeaders["Content-Type"] = "text/html";
}

void HttpSocket::sendHeaders(void)
{
  _buffWrite.append("HTTP/1.1 ");
  _buffWrite.append(Utils::toString(_statusCode));
  _buffWrite.append(" ");
  _buffWrite.append(_reason);
  _buffWrite.append("\n");

  for (std::map<std::string, std::string>::const_iterator it = _repHeaders.begin();
      it != _repHeaders.end(); ++it)
  {
    _buffWrite.append((*it).first);
    _buffWrite.append(": ");
    _buffWrite.append((*it).second);
    _buffWrite.append("\n");
  }
  _buffWrite.append("\n");
  _headersSent = true;
}

void HttpSocket::send(std::string const & data)
{
  if (_headersSent == false)
    sendHeaders();
  _buffWrite.append(data);
}

void HttpSocket::end(std::string const & data)
{
  if (_headersSent == false)
    sendHeaders();
  _buffWrite.append(data);
  _closeOnWrite = true;
}

void HttpSocket::writeAvailable(void)
{
  Socket::writeAvailable();

  if (_closeOnWrite && _buffWrite.size() == 0)
    _close();
}

void HttpSocket::errAvailable(void)
{
  Socket::errAvailable();
}

void HttpSocket::onBody(void)
{
}

void HttpSocket::reqRecieved(void)
{
  std::cout << "Method : " << _method << std::endl;
  std::cout << "Path : " << _path << std::endl;

  for (std::map<std::string, std::string>::const_iterator it = _headers.begin();
      it != _headers.end(); ++it)
  {
    std::cout << "'" << (*it).first << "' : '" << (*it).second << "'" << std::endl;
  }
  std::cout << "-----------------------------------------------------" << std::endl;
  end("hey <strong>!!!</strong>");
}

void HttpSocket::setStatusCode(int statusCode)
{
  _statusCode = statusCode;
}

int HttpSocket::getStatusCode(void) const
{
  return _statusCode;
}

void HttpSocket::setReason(std::string const & reason)
{
  _reason = reason;
}

std::string const & HttpSocket::getReason(void) const
{
  return _reason;
}
