
#pragma   once

#include "BaseClass.hh"

namespace Net
{
  class SelectEntity : public BaseClass
  {
    private:
      int     _fd;
      bool    _closed;
      bool    _deleteOnClose;

    public:
      SelectEntity(void);
      SelectEntity(int fd);
      virtual ~SelectEntity(void);

      virtual void  readAvailable(void) = 0;
      virtual void  writeAvailable(void) = 0;
      virtual void  errAvailable(void) = 0;
      virtual void  defaultCallback(void);

      int   getFd(void) const;
      bool  closed(void) const;
      bool  getDeleteOnClose(void) const;

    protected:
      void  setFd(int fd);
      void  setDeleteOnClose(bool deleteOnClose = true);
      void  setClosed(bool closed_ = true);
  };
}
