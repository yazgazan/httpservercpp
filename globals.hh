
#pragma   once

#include  <string>

namespace Net
{
  enum  { TIMEOUT_WAIT = -1, NO_TIMEOUT = 0 };
  int const         default_backLog = 5;
  int const         default_max_read = 1024;

  std::string const e_fail_alloc = "Can't allocate memory";
  std::string const e_socket_fail = "Socket (2) failed";
  std::string const e_socket_alread_initialized = "Socket already initialized";
  std::string const e_port_undefined = "Can't bind, port undefined";
  std::string const e_already_running_port = "Can't set port, server is running";
  std::string const e_already_running_start = "Can't start server, already running";
  std::string const e_already_running_bind = "Can't bind, already running";
  std::string const e_already_running_listen = "Can't listen, already running";
  std::string const e_already_running_backlog = "Can't set backlog, server is running";
  std::string const e_already_running_reuseaddr = "Can't set reuseAddr, server is running";
  std::string const e_bind_not_ok = "Can't listen, bind failed";
  std::string const e_not_running_select = "Can't select, server is not running";
  std::string const e_not_running_accept = "Can't accept, server is not running";
  std::string const e_no_entity = "Can't run select loop, no entities";
  std::string const e_buff_already_allocated = "Can't set buffSize, buffer already allocated";
}

