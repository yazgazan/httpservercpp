
MAKE=		make -C

LOGGER=		tests/logger
NETCLIENT=	tests/netclient
PROXY=		tests/proxy



all:	logger netclient proxy

clean:	logger_clean netclient_clean proxy_clean

fclean:	clean logger_fclean netclient_fclean proxy_fclean

re:	logger_re netclient_re proxy_re

logger:
	$(MAKE) $(LOGGER)

logger_clean:
	$(MAKE) $(LOGGER) clean

logger_fclean:
	$(MAKE) $(LOGGER) fclean

logger_re:
	$(MAKE) $(LOGGER) re

netclient:
	$(MAKE) $(NETCLIENT)

netclient_clean:
	$(MAKE) $(NETCLIENT) clean

netclient_fclean:
	$(MAKE) $(NETCLIENT) fclean

netclient_re:
	$(MAKE) $(NETCLIENT) re

proxy:
	$(MAKE) $(PROXY)

proxy_clean:
	$(MAKE) $(PROXY) clean

proxy_fclean:
	$(MAKE) $(PROXY) fclean

proxy_re:
	$(MAKE) $(PROXY) re

.PHONY:		all clean fclean re logger logger_clean logger_fclean logger_re netclient netclient_clean netclient_fclean netclient_re

