
#pragma   once

#include  <string>

#include  "globals.hh"
#include  "Select.hh"
#include  "SocketEntity.hh"
#include  "Socket.hh"

namespace Net
{
  template <class T_Socket=Socket>
  class Server : public SocketEntity
  {
    private:
      short               _port;
      int                 _backLog;
      Select              _selector;

      bool                _running;
      bool                _bindOk;
      bool                _reuseAddr;

    public:
      Server(short port);
      Server(void);
      ~Server(void);

      void                setPort(short port);
      short               getPort(void) const;
      void                setBackLog(int backLog);
      int                 getBackLog(void) const;
      void                setReuseAddr(bool reuseAddr = true);
      bool                getReuseAddr(void) const;

      void                startServer(void);

      virtual void        readAvailable(void);
      virtual void        writeAvailable(void);
      virtual void        errAvailable(void);
      virtual bool        acceptCallback(T_Socket *newSocket);

    private:
      void                _bind(void);
      void                _listen(void);
      T_Socket *          _accept(void) const;
      virtual void        onError(void) const;
  };
}

#include  "Server.cpp"

