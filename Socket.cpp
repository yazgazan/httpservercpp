
#include  <netinet/in.h>
#include  <arpa/inet.h>

#include  <errno.h>
#include  <string.h>
#include  <unistd.h>

#include  <cstdlib>
#include  <iostream>

#include  "Socket.hh"
#include  "globals.hh"

using namespace Net;

Socket::Socket(void)
  : SocketEntity(),
    _maxRead(default_max_read),
    _buff(),
    _buffWrite(),
    _ip(),
    _port(0),
    _sin()
{
}

Socket::Socket(int fd, struct sockaddr_in const & sin)
  : SocketEntity(fd),
    _maxRead(default_max_read),
    _buff(),
    _buffWrite(),
    _ip(inet_ntoa(sin.sin_addr)),
    _port(ntohs(sin.sin_port)),
    _sin(sin)
{
}

Socket::Socket(Socket const & cp)
  : SocketEntity(cp.getFd()),
    _maxRead(default_max_read),
    _buff(cp.getBuff()),
    _buffWrite(cp.getBuffWrite()),
    _ip(cp.getIp()),
    _port(cp.getPort()),
    _sin(cp.getSin())
{
}

Socket::Socket(std::string const & ip, short port)
  : SocketEntity(),
    _maxRead(default_max_read),
    _buff(),
    _buffWrite(),
    _ip(ip),
    _port(port),
    _sin()
{
  _connect();
}

Socket::~Socket(void)
{
  if (getFd() != -1 && !closed())
    _close();
}

void Socket::readAvailable(void)
{
  ssize_t read_;
  char  *buffRead = new char[_maxRead];

  if (isError())
    return;
  if (closed())
    return;
  read_ = read(getFd(), buffRead, _maxRead);
  if (read_ == -1)
  {
    _setError(std::string(strerror(errno)));
    delete[] buffRead;
    return;
  }
  if (read_ == 0)
  {
    setClosed();
    delete[] buffRead;
    return;
  }
  _buff.append(buffRead, read_);
  delete[] buffRead;
}

void Socket::writeAvailable(void)
{
  ssize_t     wrote;
  std::string tmp;

  if (isError())
    return;
  if (closed())
    return;
  if (_buffWrite.size() == 0)
    return;

  wrote = write(getFd(), _buffWrite.data(), _buffWrite.size());
  if (wrote == -1)
  {
    _setError(std::string(strerror(errno)));
    return;
  }
  if (wrote == 0)
    return;
  tmp = _buffWrite.substr(wrote, std::string::npos);
  _buffWrite = tmp;
}

void Socket::errAvailable(void)
{
  if (closed())
    return;
}

struct sockaddr_in const & Socket::getSin(void) const
{
  return _sin;
}

std::string const & Socket::getBuff(void) const
{
  return _buff;
}

std::string const & Socket::getBuffWrite(void) const
{
  return _buffWrite;
}

std::string const & Socket::getIp(void) const
{
  return _ip;
}

short Socket::getPort(void) const
{
  return _port;
}

void Socket::setIp(std::string const & ip)
{
  _ip = ip;
}

void Socket::setPort(short port)
{
  _port = port;
}

void Socket::_connect(std::string const & ip, short port)
{
  if (isError())
    return;
  _ip = ip;
  _port = port;
  _connect();
}

void Socket::_connect()
{
  int err;

  if (isError())
    return;
  if (getFd() == -1)
    _initSocket();
  _sin.sin_addr.s_addr = inet_addr(_ip.c_str());
  _sin.sin_family = AF_INET;
  _sin.sin_port = htons(_port);
  err = connect(getFd(), (struct sockaddr*)&_sin, sizeof(_sin));
  if (err == -1)
  {
    _setError(std::string(strerror(errno)));
    return;
  }
  setClosed(false);
}

std::string *Socket::readSync(int maxRead)
{
  ssize_t     read_;
  char        *buffRead;
  std::string *ret = NULL;

  if (isError())
    return NULL;
  if (closed())
    return ret;
  if (maxRead == 0)
    maxRead = _maxRead;

  buffRead = new char[maxRead];
  if (!buffRead)
  {
    _setError(e_fail_alloc);
    return NULL;
  }
  read_ = read(getFd(), buffRead, maxRead);
  if (read_ == -1)
  {
    _setError(std::string(strerror(errno)));
    delete[] buffRead;
    return NULL;
  }
  if (read_ == 0)
  {
    setClosed();
    delete[] buffRead;
    return NULL;
  }
  ret = new std::string(buffRead, read_);
  delete[] buffRead;
  return ret;
}

std::string *Socket::writeSync(std::string const & data) const
{
  ssize_t     wrote;

  if (isError())
    return NULL;
  if (closed())
    return NULL;

  wrote = write(getFd(), data.data(), data.size());
  if (wrote == -1)
  {
    _setError(std::string(strerror(errno)));
    return NULL;
  }
  return (new std::string(data.substr(wrote, std::string::npos)));
}

