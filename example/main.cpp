
#include  <iostream>

#include  "MyServer.hh"
#include  "Socket.hh"

int main(void)
{
  MyServer toto;

  toto.setPort(12345);
  toto.setReuseAddr();
  toto.setBackLog(2);
  toto.startServer();

  if (toto.isError())
    std::cerr << "Error : " << toto.getError() << std::endl;
  else
    std::cout << "Everything is ok" << std::endl;
  return 0;
}

