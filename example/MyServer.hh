
#pragma   once

#include  "Server.hh"
#include  "MySocket.hh"

class MyServer : public Net::Server<MySocket>
{
  private:
  public:
    bool acceptCallback(MySocket *newSocket);
};

