
#include  <iostream>

#include  "MySocket.hh"

MySocket::MySocket(void)
  : Net::Socket()
{
}

MySocket::MySocket(int socket, struct sockaddr_in const & csin)
  : Net::Socket(socket, csin)
{
}

void MySocket::readAvailable(void)
{
  Net::Socket::readAvailable();

  std::cout << _buff << std::endl;
  if (_buff.find("\r\n.\r\n") != std::string::npos)
    _close();
  else if (_buff.find("\n.\n") != std::string::npos)
    _close();
  _buff.clear();
  _buffWrite.append("OK !!!\r\n");
}

void MySocket::errAvailable(void)
{
  std::cout << "MySocket::errAvailable" << std::endl;
}

