
#pragma   once

#include  "Socket.hh"

class MySocket : public Net::Socket
{
  private:
  public:
    MySocket(void);
    MySocket(int socket, struct sockaddr_in const & csin);

    void readAvailable(void);
    void errAvailable(void);
};

