
#include <cstdlib>
#include <iostream>

#include "BaseClass.hh"

using namespace Net;

BaseClass::BaseClass(void)
  : _lastError(""),
    _error(false)
{

}

BaseClass::~BaseClass(void)
{

}

void BaseClass::_setError(std::string const & err) const
{
  _error = true;
  _lastError = err;
  onError();
}

void BaseClass::onError(void) const
{
  std::cerr << "Error : " << _lastError << std::endl;
  exit(1);
}

bool BaseClass::isError(void) const
{
  return _error;
}

std::string const & BaseClass::getError(void) const
{
  return _lastError;
}

void BaseClass::ignoreError(void)
{
  _error = false;
  _lastError = "";
}
