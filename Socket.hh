
#pragma   once

#include  <netinet/in.h>

#include  <unistd.h>

#include  <string>

#include  "globals.hh"
#include  "SelectEntity.hh"
#include  "SocketEntity.hh"

namespace Net
{
  class Socket : public SocketEntity
  {
    protected:
      unsigned int                _maxRead;
      std::string                 _buff;
      std::string                 _buffWrite;
      std::string                 _ip;
      short                       _port;

    private:
      struct sockaddr_in          _sin;

    public:
      Socket(void);
      Socket(int fd, struct sockaddr_in const & sin);
      Socket(Socket const & cp);
      Socket(std::string const & ip, short port);
      virtual ~Socket(void);

      virtual void                readAvailable(void);
      virtual void                writeAvailable(void);
      virtual void                errAvailable(void);

      struct sockaddr_in const &  getSin(void) const;
      std::string const &         getBuff(void) const;
      std::string const &         getBuffWrite(void) const;
      std::string const &         getIp(void) const;
      short                       getPort(void) const;
      void                        setIp(std::string const & ip);
      void                        setPort(short port);

      void                        _connect(std::string const & ip, short port);
      void                        _connect(void);

      /* sync functions */
      std::string                 *readSync(int maxRead = 0);
      std::string                 *writeSync(std::string const & data) const;
  };
}

