
#include  "SelectEntity.hh"

using namespace Net;

SelectEntity::SelectEntity(void)
  : _fd(-1),
    _closed(true),
    _deleteOnClose(false)
{
}

SelectEntity::SelectEntity(int fd)
  : _fd(fd),
    _closed(false),
    _deleteOnClose(false)
{
}

SelectEntity::~SelectEntity(void)
{
}

void SelectEntity::defaultCallback(void)
{
}

int   SelectEntity::getFd(void) const
{
  return _fd;
}

void  SelectEntity::setFd(int fd)
{
  _fd = fd;
}

bool SelectEntity::closed(void) const
{
  return _closed;
}

bool SelectEntity::getDeleteOnClose(void) const
{
  return _deleteOnClose;
}

void SelectEntity::setDeleteOnClose(bool deleteOnClose)
{
  _deleteOnClose = deleteOnClose;
}

void SelectEntity::setClosed(bool closed_)
{
  _closed = closed_;
}
