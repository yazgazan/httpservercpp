
#include  <netinet/in.h>

#include  <unistd.h>

#include "SocketEntity.hh"

using namespace Net;

SocketEntity::SocketEntity(void)
  : SelectEntity()
{

}

SocketEntity::SocketEntity(int socket)
  : SelectEntity(socket)
{

}

void SocketEntity::_close(void)
{
  close(getFd());
  setFd(-1);
  setClosed();
}

void SocketEntity::_initSocket(void)
{
  if (isError())
    return;
  if (getFd() != -1)
  {
    _setError(e_socket_alread_initialized);
    return;
  }
  setFd(socket(AF_INET, SOCK_STREAM, 0));
  if (getFd() == -1)
  {
    _setError(e_socket_fail);
    return;
  }
  setClosed(false);
}
