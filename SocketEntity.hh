
#pragma once

#include  "globals.hh"
#include  "SelectEntity.hh"

namespace Net
{
  class SocketEntity : public SelectEntity
  {
    public:
      SocketEntity(void);
      SocketEntity(int socket);

      void _close(void);

    protected:
      void _initSocket(void);
  };
}

