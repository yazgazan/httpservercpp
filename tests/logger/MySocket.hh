
#pragma   once

#include  "Socket.hh"
#include  "MyServer.hh"

class MyServer;

class MySocket : public Net::Socket
{
  private:
    MyServer  * _server;
    bool        _closeWhenWriteEnded;

  public:
    MySocket(void);
    MySocket(int socket, struct sockaddr_in const & csin);
    ~MySocket(void);

    void setServer(MyServer * server);

    void readAvailable(void);
    void writeAvailable(void);
    void errAvailable(void);
    void closed(void);
};

