
#include  <iostream>

#include  "MyServer.hh"

MyServer::MyServer(void)
  : Net::Server<MySocket>(),
    _logfile("log.txt", std::ofstream::app)
{
}

bool MyServer::acceptCallback(MySocket *newSocket)
{
  newSocket->setServer(this);

  return true;
}

void MyServer::logBuff(std::string const & buff)
{
  if (!_logfile.is_open())  {
    std::cerr << "Couldn't log buff, no logfile open." << std::endl;
    return;
  }
  _logfile << buff;
  _logfile.flush();
}
