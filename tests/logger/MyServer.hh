
#pragma   once

#include  <fstream>
#include  <string>

#include  "Server.hh"
#include  "MySocket.hh"

class MySocket;

class MyServer : public Net::Server<MySocket>
{
  private:
    std::ofstream   _logfile;

  public:
    MyServer(void);
    bool acceptCallback(MySocket *newSocket);
    void  logBuff(std::string const & buff);
};

