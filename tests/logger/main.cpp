
#include  "MyServer.hh"
#include  "Socket.hh"

int main(void)
{
  MyServer logger;

  logger.setPort(1234);
  logger.setReuseAddr();
  logger.setBackLog(5);
  logger.startServer();

  return 0;
}

