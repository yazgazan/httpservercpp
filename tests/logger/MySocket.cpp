
#include  <iostream>

#include  "MySocket.hh"

MySocket::MySocket(void)
  : Net::Socket(),
    _server(NULL),
    _closeWhenWriteEnded(false)
{
}

MySocket::MySocket(int socket, struct sockaddr_in const & csin)
  : Net::Socket(socket, csin),
    _server(NULL),
    _closeWhenWriteEnded(false)
{
}

void MySocket::readAvailable(void)
{
  Net::Socket::readAvailable();

  if (_buff.find("\r\n.\r\n") != std::string::npos)
  {
    _buffWrite.append("goodbye ;)\n");
    _closeWhenWriteEnded = true;
  }
  else if (_buff.find("\n.\n") != std::string::npos)
  {
    _buffWrite.append("goodbye ;)\n");
    _closeWhenWriteEnded = true;
  }
}

void MySocket::writeAvailable(void)
{
  Net::Socket::writeAvailable();

  if (_closeWhenWriteEnded && _buffWrite.size() == 0)
    _close();
}

void MySocket::errAvailable(void)
{
  std::cout << "MySocket::errAvailable" << std::endl;
}

void MySocket::closed(void)
{
  _server->logBuff(_buff);
}

MySocket::~MySocket(void)
{
}

void MySocket::setServer(MyServer * server)
{
  _server = server;
}
