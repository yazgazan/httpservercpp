
#include  <iostream>

#include  "Proxy.hh"

Proxy::Proxy(void)
  : Net::Server<HttpSocket>()
{
}

bool Proxy::acceptCallback(HttpSocket *newSocket)
{
  (void)newSocket;

  return true;
}

