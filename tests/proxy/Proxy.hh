
#pragma   once

#include  <fstream>
#include  <string>

#include  "Server.hh"
#include  "HttpSocket.hh"

class Proxy : public Net::Server<HttpSocket>
{
  public:
    Proxy(void);
    bool acceptCallback(HttpSocket *newSocket);
};

