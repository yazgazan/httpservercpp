
#include  "Proxy.hh"
#include  "Socket.hh"

int main(void)
{
  Proxy proxy;

  proxy.setPort(8080);
  proxy.setReuseAddr();
  proxy.startServer();
  return 0;
}

