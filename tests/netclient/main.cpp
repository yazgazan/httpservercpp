
#include <iostream>

#include "MySocket.hh"

int main(void)
{
  MySocket client("127.0.0.1", 1234);

  std::cout << client.writeSync("hey !\n.\n")->size() << std::endl;
  std::cout << *client.readSync() << std::endl;
  return 0;
}

