
#pragma   once

#include <string>

#include "Socket.hh"

class MySocket : public Net::Socket
{
  public:
    MySocket(std::string const & ip, short port);
};

