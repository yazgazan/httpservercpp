
#pragma once

#include <string>

#include "globals.hh"

namespace Net
{
  class BaseClass
  {
    private:
      mutable std::string   _lastError;
      mutable bool          _error;

    protected:
      BaseClass(void);
      virtual ~BaseClass(void);

      void                  _setError(std::string const & err) const;
      virtual void          onError(void) const;

    public:
      bool                  isError(void) const;
      std::string const &   getError(void) const;
      void                  ignoreError(void);
  };
}

