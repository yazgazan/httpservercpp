
#include  <sys/types.h>
#include  <sys/socket.h>
#include  <netinet/in.h>

#include  <errno.h>
#include  <string.h>
#include  <unistd.h>

#include  <cstdlib>
#include  <iostream>

using namespace Net;

template<class T_Socket>
Server<T_Socket>::Server(short port)
  : SocketEntity(),
    _port(port),
    _backLog(default_backLog),
    _selector(),
    _running(false),
    _bindOk(false),
    _reuseAddr(false)
{
  _initSocket();
  startServer();
}

template<class T_Socket>
Server<T_Socket>::Server(void)
  : SocketEntity(),
    _port(0),
    _backLog(default_backLog),
    _selector(),
    _running(false),
    _bindOk(false),
    _reuseAddr(false)
{
  _initSocket();
}

template<class T_Socket>
Server<T_Socket>::~Server(void)
{
  if (getFd() != -1)
    close(getFd());
  _selector.removeEntity(this);
}

template<class T_Socket>
void Server<T_Socket>::setPort(short port)
{
  if (isError())
    return;
  if (_bindOk)
  {
    _setError(e_already_running_port);
    return;
  }
  _port = port;
}

template<class T_Socket>
short Server<T_Socket>::getPort(void) const
{
  return _port;
}

template<class T_Socket>
void Server<T_Socket>::setBackLog(int backLog)
{
  if (isError())
    return;
  if (_running)
  {
    _setError(e_already_running_backlog);
    return;
  }
  _backLog = backLog;
}

template<class T_Socket>
int Server<T_Socket>::getBackLog(void) const
{
  return _backLog;
}

template<class T_Socket>
void Server<T_Socket>::setReuseAddr(bool reuseAddr)
{
  if (_running)
  {
    _setError(e_already_running_reuseaddr);
    return;
  }
  _reuseAddr = reuseAddr;
}

template<class T_Socket>
bool Server<T_Socket>::getReuseAddr(void) const
{
  return _reuseAddr;
}

template<class T_Socket>
void Server<T_Socket>::startServer(void)
{
  if (isError())
    return;
  if (_running)
  {
    _setError(e_already_running_start);
    return;
  }
  _bind();
  _listen();
  _selector.addEntity(this);
  _running = true;
  _selector.run();
}

  template<class T_Socket>
void  Server<T_Socket>::readAvailable(void)
{
  T_Socket *newSocket;

  newSocket = _accept();

  if (isError())
    return;
  if (newSocket == NULL)
    return;
  if (acceptCallback(newSocket))
    _selector.addEntity(newSocket);
  else
  {
    newSocket->_close();
    delete newSocket;
  }
}

  template<class T_Socket>
void  Server<T_Socket>::writeAvailable(void)
{
  std::cout << "Server::writeAvailable" << std::endl;
}

  template<class T_Socket>
void  Server<T_Socket>::errAvailable(void)
{
  std::cout << "Server::errAvailable" << std::endl;
}

  template<class T_Socket>
bool Server<T_Socket>::acceptCallback(T_Socket *newSocket)
{
  (void)newSocket;

  return true;
}

  template<class T_Socket>
void Server<T_Socket>::_bind(void)
{
  int                 err;
  struct sockaddr_in  sin;

  if (isError())
    return;
  if (_port == 0)
  {
    _setError(e_port_undefined);
    return;
  }
  if (_running)
  {
    _setError(e_already_running_bind);
    return;
  }

  _bindOk = false;
  sin.sin_addr.s_addr = htonl(INADDR_ANY);
  sin.sin_family = AF_INET;
  sin.sin_port = htons(_port);
  if (_reuseAddr)
  {
    int const yes = 1;

    err = setsockopt(getFd(), SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
    if (err == -1)
    {
      _setError(std::string(strerror(errno)));
      return;
    }
  }
  err = bind(getFd(), (struct sockaddr*)&sin, sizeof(sin));

  if (err == -1)
  {
    _setError(std::string(strerror(errno)));
    return;
  }
  _bindOk = true;
}

  template<class T_Socket>
void Server<T_Socket>::_listen(void)
{
  int         err;

  if (isError())
    return;
  if (_running)
  {
    _setError(e_already_running_listen);
    return;
  }
  if (!_bindOk)
  {
    _setError(e_bind_not_ok);
    return;
  }

  err = listen(getFd(), _backLog);

  if (err == -1)
  {
    _setError(std::string(strerror(errno)));
    return;
  }
  _running = true;
}

template<class T_Socket>
T_Socket *Server<T_Socket>::_accept(void) const
{
  int                 newSocket;
  struct sockaddr_in  csin;
  socklen_t           csinSize;

  if (isError())
    return NULL;
  if (!_running)
  {
    _setError(e_not_running_accept);
    return NULL;
  }
  csinSize = sizeof(csin);
  newSocket = accept(getFd(), (struct sockaddr*)&csin, &csinSize);
  return (new T_Socket(newSocket, csin));
}

template<class T_Socket>
void Server<T_Socket>::onError(void) const
{
  std::cerr << "Error : " << getError() << std::endl;
  exit(1);
}
