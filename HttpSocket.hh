
#pragma once

#include  <map>
#include  <string>

#include  "Socket.hh"

namespace Net
{
  class HttpSocket : public Socket
  {
    private:
      std::map<std::string, std::string>  _headers;
      std::string                         _method;
      std::string                         _path;
      bool                                _headersRecieved;
      int                                 _statusCode;
      std::string                         _reason;
      bool                                _closeOnWrite;
      bool                                _headersSent;

    protected:
      std::map<std::string, std::string>  _repHeaders;

    public:
      HttpSocket(int fd, struct sockaddr_in const & sin);
      virtual ~HttpSocket(void);

      void                        readAvailable(void);
      void                        writeAvailable(void);
      virtual void                errAvailable(void);
      virtual void                onBody(void);
      virtual void                reqRecieved(void);

      void                        parseHeader(void);
      void                        parseFirstLine(std::string const & line);
      void                        parseHeaderFields(std::string const & line);
      void                        initResHeaders(void);
      void                        sendHeaders(void);
      void                        send(std::string const & data);
      void                        end(std::string const & data = "");

      void                        setStatusCode(int statusCode);
      int                         getStatusCode(void) const;
      void                        setReason(std::string const & reason);
      std::string const &         getReason(void) const;
  };
}

